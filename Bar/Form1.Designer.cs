﻿namespace Bar
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ButtonServir = new System.Windows.Forms.Button();
            this.ComboBoxBebidas = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.ButtonEncher = new System.Windows.Forms.Button();
            this.ButtonEsvaziar = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.LabelQtd = new System.Windows.Forms.Label();
            this.progressBarLiquido = new System.Windows.Forms.ProgressBar();
            this.LabelCCheio = new System.Windows.Forms.Label();
            this.LabelStatus = new System.Windows.Forms.Label();
            this.ComboBoxCopo = new System.Windows.Forms.ComboBox();
            this.LabelCopo = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // ButtonServir
            // 
            this.ButtonServir.Location = new System.Drawing.Point(71, 356);
            this.ButtonServir.Name = "ButtonServir";
            this.ButtonServir.Size = new System.Drawing.Size(196, 23);
            this.ButtonServir.TabIndex = 0;
            this.ButtonServir.Text = "Copo Na Mesa";
            this.ButtonServir.UseVisualStyleBackColor = true;
            this.ButtonServir.Click += new System.EventHandler(this.ButtonServir_Click);
            // 
            // ComboBoxBebidas
            // 
            this.ComboBoxBebidas.FormattingEnabled = true;
            this.ComboBoxBebidas.Items.AddRange(new object[] {
            "Cerveja",
            "Agua",
            "Sangria",
            "Refrigerante"});
            this.ComboBoxBebidas.Location = new System.Drawing.Point(71, 105);
            this.ComboBoxBebidas.Name = "ComboBoxBebidas";
            this.ComboBoxBebidas.Size = new System.Drawing.Size(196, 21);
            this.ComboBoxBebidas.TabIndex = 1;
            this.ComboBoxBebidas.SelectedIndexChanged += new System.EventHandler(this.ComboBoxBebidas_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(246, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(119, 26);
            this.label1.TabIndex = 3;
            this.label1.Text = "Bar CET30";
            // 
            // ButtonEncher
            // 
            this.ButtonEncher.Enabled = false;
            this.ButtonEncher.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonEncher.Location = new System.Drawing.Point(392, 171);
            this.ButtonEncher.Name = "ButtonEncher";
            this.ButtonEncher.Size = new System.Drawing.Size(75, 42);
            this.ButtonEncher.TabIndex = 4;
            this.ButtonEncher.Text = "+";
            this.ButtonEncher.UseVisualStyleBackColor = true;
            this.ButtonEncher.Click += new System.EventHandler(this.ButtonEncher_Click);
            // 
            // ButtonEsvaziar
            // 
            this.ButtonEsvaziar.Enabled = false;
            this.ButtonEsvaziar.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonEsvaziar.Location = new System.Drawing.Point(392, 242);
            this.ButtonEsvaziar.Name = "ButtonEsvaziar";
            this.ButtonEsvaziar.Size = new System.Drawing.Size(75, 42);
            this.ButtonEsvaziar.TabIndex = 5;
            this.ButtonEsvaziar.Text = "-";
            this.ButtonEsvaziar.UseVisualStyleBackColor = true;
            this.ButtonEsvaziar.Click += new System.EventHandler(this.ButtonEsvaziar_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(68, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Bebida";
            // 
            // LabelQtd
            // 
            this.LabelQtd.AutoSize = true;
            this.LabelQtd.Enabled = false;
            this.LabelQtd.Location = new System.Drawing.Point(428, 321);
            this.LabelQtd.Name = "LabelQtd";
            this.LabelQtd.Size = new System.Drawing.Size(19, 13);
            this.LabelQtd.TabIndex = 7;
            this.LabelQtd.Text = "- - ";
            // 
            // progressBarLiquido
            // 
            this.progressBarLiquido.Location = new System.Drawing.Point(348, 356);
            this.progressBarLiquido.Name = "progressBarLiquido";
            this.progressBarLiquido.Size = new System.Drawing.Size(192, 23);
            this.progressBarLiquido.TabIndex = 8;
            // 
            // LabelCCheio
            // 
            this.LabelCCheio.AutoSize = true;
            this.LabelCCheio.Location = new System.Drawing.Point(392, 105);
            this.LabelCCheio.Name = "LabelCCheio";
            this.LabelCCheio.Size = new System.Drawing.Size(0, 13);
            this.LabelCCheio.TabIndex = 9;
            // 
            // LabelStatus
            // 
            this.LabelStatus.AutoSize = true;
            this.LabelStatus.Location = new System.Drawing.Point(71, 407);
            this.LabelStatus.Name = "LabelStatus";
            this.LabelStatus.Size = new System.Drawing.Size(0, 13);
            this.LabelStatus.TabIndex = 10;
            this.LabelStatus.Visible = false;
            // 
            // ComboBoxCopo
            // 
            this.ComboBoxCopo.FormattingEnabled = true;
            this.ComboBoxCopo.Items.AddRange(new object[] {
            "22",
            "33",
            "45",
            "50"});
            this.ComboBoxCopo.Location = new System.Drawing.Point(71, 221);
            this.ComboBoxCopo.Name = "ComboBoxCopo";
            this.ComboBoxCopo.Size = new System.Drawing.Size(196, 21);
            this.ComboBoxCopo.TabIndex = 11;
            this.ComboBoxCopo.Visible = false;
            this.ComboBoxCopo.SelectedIndexChanged += new System.EventHandler(this.ComboBoxCopo_SelectedIndexChanged);
            // 
            // LabelCopo
            // 
            this.LabelCopo.AutoSize = true;
            this.LabelCopo.Location = new System.Drawing.Point(71, 183);
            this.LabelCopo.Name = "LabelCopo";
            this.LabelCopo.Size = new System.Drawing.Size(32, 13);
            this.LabelCopo.TabIndex = 12;
            this.LabelCopo.Text = "Copo";
            this.LabelCopo.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(594, 444);
            this.Controls.Add(this.LabelCopo);
            this.Controls.Add(this.ComboBoxCopo);
            this.Controls.Add(this.LabelStatus);
            this.Controls.Add(this.LabelCCheio);
            this.Controls.Add(this.progressBarLiquido);
            this.Controls.Add(this.LabelQtd);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ButtonEsvaziar);
            this.Controls.Add(this.ButtonEncher);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ComboBoxBebidas);
            this.Controls.Add(this.ButtonServir);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ButtonServir;
        private System.Windows.Forms.ComboBox ComboBoxBebidas;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button ButtonEncher;
        private System.Windows.Forms.Button ButtonEsvaziar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label LabelQtd;
        private System.Windows.Forms.ProgressBar progressBarLiquido;
        private System.Windows.Forms.Label LabelCCheio;
        private System.Windows.Forms.Label LabelStatus;
        private System.Windows.Forms.ComboBox ComboBoxCopo;
        private System.Windows.Forms.Label LabelCopo;
    }
}

