﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bar
{
    public partial class Form1 : Form
    {
        private Copo EncheCopo;
        private bool EncheLimpa;
        

        public Form1()
        {
            InitializeComponent();
            EncheCopo = new Copo();
            EncheLimpa = false; 
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void ComboBoxBebidas_SelectedIndexChanged(object sender, EventArgs e)
        {
            LabelCopo.Visible = true;
            ComboBoxCopo.Visible = true;
        }

        private void ComboBoxCopo_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void ButtonServir_Click(object sender, EventArgs e)
        {
            if (!EncheLimpa)
            {
                
                if (ComboBoxBebidas.Text == "") // Verifica se foi escolhido um liquido
                {
                    LabelStatus.Visible = true;
                    LabelStatus.Text = "Escolhe uma bebida!";
                }
                else if (ComboBoxCopo.Text == "") // Verifica se foi escolhido uma quantidade
                {
                    LabelStatus.Visible = true;
                    LabelStatus.Text = "Escolhe um Copo!";
                }
                else
                {
                    EncheCopo.Capacidade = Convert.ToDouble(ComboBoxCopo.Text.ToString());
                    LabelQtd.Enabled = true;
                    EncheCopo.Liquido = ComboBoxBebidas.Text;
                    LabelQtd.Text = EncheCopo.Contem.ToString();
                    ButtonEncher.Enabled = true;
                    ButtonEsvaziar.Enabled = true;
                    progressBarLiquido.Minimum = 0;
                    progressBarLiquido.Maximum = 100;
                    LabelStatus.Visible = true;
                    LabelStatus.Text = "O copo de " + EncheCopo.Capacidade.ToString() + " cl contem " + EncheCopo.Contem.ToString() + " cl " + "de " + EncheCopo.Liquido;
                    EncheLimpa = true;
                    ButtonServir.Text = "Limpar a Mesa!";
                   
                }
            }
            else
            {
                EncheLimpa = false;
                LabelCopo.Visible = false;
                ComboBoxCopo.Visible = false;
                ComboBoxBebidas.Text = "";
                ComboBoxCopo.Text = "";
                ButtonEncher.Enabled = false;
                ButtonEsvaziar.Enabled = false;
                progressBarLiquido.Value = 0;
                LabelQtd.Enabled = false;
                LabelQtd.Text = "- -";
                LabelStatus.Visible = false;
                EncheCopo.Contem = 0;
                ButtonServir.Text = "Copo Na Mesa";
            }
        }

        private void ButtonEncher_Click(object sender, EventArgs e)
        {
            EncheCopo.Encher(1);
            LabelQtd.Text = EncheCopo.Contem.ToString();

            progressBarLiquido.Value = EncheCopo.ValorEmPercentagem();
            LabelStatus.Visible = true;
            if (EncheCopo.Cheio())
            {
                LabelStatus.Text = "O copo está cheio!";
            }
            else
            LabelStatus.Text = "O copo de " + EncheCopo.Capacidade.ToString() + " cl contem " + EncheCopo.Contem.ToString() + " cl " + "de " + EncheCopo.Liquido;

           
        }

        private void ButtonEsvaziar_Click(object sender, EventArgs e)
        {
            EncheCopo.Esvaziar(1);
            LabelQtd.Text = EncheCopo.Contem.ToString();
            
            progressBarLiquido.Value = EncheCopo.ValorEmPercentagem();
            LabelStatus.Visible = true;
            LabelStatus.Text = "O copo de " + EncheCopo.Capacidade.ToString() + " cl contem " +  EncheCopo.Contem.ToString() + " cl " + "de " + EncheCopo.Liquido;
        }

       
    }
}
